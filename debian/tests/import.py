from __future__ import print_function

import persistent
print(persistent)

# Make sure the extension modules can be imported.

import persistent._timestamp
print(persistent._timestamp)

import persistent.cPersistence
print(persistent.cPersistence)

import persistent.cPickleCache
print(persistent.cPickleCache)
